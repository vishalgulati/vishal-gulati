import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: '<h1>AVON test app on Azure.</h1>'
})
export class AppComponent { }
